# Nach dem ich nicht wirklich fündig geworden bin, ein Youtube Downloader mit Gui für Linux zu bekommen, hab ich selbst ein Bash Skript # für Youtube-DL geschrieben. Die grafische ausgabe erledigt Zenity. Zenity wird dafür benötigt. FFmpeg kommt ja von Youtube-dl.
# Der Quellcode für die die das auch gerne möchten:

#!/bin/bash
app2=Youtube-dl-video

musik_dl() {

URL=$(zenity --entry --text "Hier URL einfügen" --title "Youtube-dl-Musik")

if     [ "$?" = "0" ]
        then
        musikdownload_start
        else
 exit 1
fi
}
musikdownload_start() {
youtube-dl -o "%(title)s.%(ext)s" -x --audio-quality 4 --newline --audio-format mp3 $URL | zenity --progress \
               --title="Youtube-dl" \
               --text="Download läuft..." \
               --pulsate \
               --auto-close \
               --auto-kill \
           
        zenity --info --width=155 \
               --title="Youtube-dl" \
               --text="Download beendet" \

}  
         
video_dl() {

URL2=$(zenity --entry --text "Hier URL einfügen" --title "Youtube-dl-Video")

if     [ "$?" = "0" ]
        then
        videodownload_start
        else
 exit 1
fi
}
videodownload_start() {
youtube-dl -o "%(title)s.%(ext)s" $URL2 | zenity --progress \
           --title="Youtube-dl" \
           --text="Download läuft..." \
           --pulsate \
           --auto-close \
           --auto-kill \
           
    zenity --info --width=155 \
           --title="Youtube-dl" \
           --text="Download beendet" \
           
} 


app=$(zenity --list --height=170 --width=300 --title="Youtube-Dl" --text "Wählen sie aus ob Video's oder Musik geladen werden soll" --column 'Downloader'  "Youtube-dl-musik" "Youtube-dl-video")
 
if    [ "$app" = "Youtube-dl-musik" ]
 then 
 musik_dl
elif  [ "$app" = "Youtube-dl-video" ]
 then
 video_dl
 else
 exit 1
      
fi