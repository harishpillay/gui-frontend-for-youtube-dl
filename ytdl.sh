#!/bin/bash
# Based on youtube video dated 14 August 2020
# https://www.youtube.com/watch?v=D0KOtLDscY4 by "Archzilla"
# Orginally in German. 
# translated into English 8 October 2020 by Harish Pillay.

app2=Youtube-dl-video

music_dl() {

URL=$(zenity --entry --text "Paste URL here" --title "Youtube-dl-Music")

if [ "$?" = "0" ]
     then
       musicdownload_start
     else
       exit 1
fi
}

musicdownload_start() {
youtube-dl -o "~/Music/%(title)s.%(ext)s" -x --audio-quality 4 --newline --audio-format mp3 $URL | zenity --progress \
               --title="Youtube-dl" \
               --text="Download started ..." \
               --pulsate \
               --auto-close \
               --auto-kill \
           
        zenity --info --width=250 \
               --title="Youtube-dl" \
               --text="Downloaded $URL to $HOME\/Music/" \

}  
         
video_dl() {

URL2=$(zenity --entry --text "Paste URL here" --title "Youtube-dl-Video")

if [ "$?" = "0" ]
     then
        videodownload_start
     else
        exit 1
fi
}

videodownload_start() {
youtube-dl -o "~/Videos/%(title)s.%(ext)s" $URL2 | zenity --progress \
           --title="Youtube-dl" \
           --text="Download started ..." \
           --pulsate \
           --auto-close \
           --auto-kill \
           
    zenity --info --width=250 \
           --title="Youtube-dl" \
           --text="Downloaded $URL2 to $HOME\/Videos/" \
           
} 

app=$(zenity --list --height=190 --width=350 --title="YouTube-DL-GUI" --radiolist --text "<b>Select whether videos or music should be loaded.</b>\n<i>Music downloads will be in $HOME/Music\nVideo downloads will be in $HOME/Videos</i>" --hide-header --column 'Type' --column "Item" FALSE "Music (saved as mp3)" FALSE "Video (saved as default)")

if [ "$app" = "Music (saved as mp3)" ]
   then 
      music_dl
elif [ "$app" = "Video (saved as default)" ]
   then
      video_dl
   else
      exit 1
fi