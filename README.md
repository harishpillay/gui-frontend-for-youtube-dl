# GUI Frontend for youtube-dl

This is a bash script that implements with zenity and youtube-dl a GUI frontend to the youtube-dl python script to download youtube videos. 

The original script was in the information section of https://www.youtube.com/watch?v=D0KOtLDscY4 and the information text was in German. The original German version is included in this repo.